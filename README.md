# README #

This is an effort to keep all my coding challenge solutions in one place.

### List of Coding challenges in this Repository? ###

* Pipes and Underscores

### Language used ###

Most of the challenges here are solved using Ruby and tested using Rspec, unless stated otherwise in each challenge's Problem Statement file.

### What do you need to do to run these? ###

* Ruby v1.9.3 +
* RSpec 3+ 

Most of the challenges were solved in Windows machine, but there is no reason why they should not run on Linux / Mac. 

### Contribution guidelines ###

* Writing tests are invited
* Code review is much appreciated


### What next? ###

* The code and tests will be updated as and when necessary for each challenge in the repository. 
* If you find no tests are only a few tests, either that is being currently written or will be pushed out soon. (Though I would like to change it in future by writing tests first)
* I am thinking of uploading architecture design diagrams for each challenge, but still in review